
import os
from dotenv import load_dotenv
from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker, scoped_session
from models.base import Base
from models import base
from models import contacts

load_dotenv()

class DBStorage:
    
    
    MC = {
        'BaseModel' : base.BaseModel,
        'Contacts' : contacts.Contacts
    }
    
    
    __engine = None
    __session = None
    
    
    def __init__(self):
        
        
        if os.getenv('DB_MODE') == 'test':
            self.__engine = create_engine("sqlite+pysqlite:///:memory:", echo=True)
        else:
            self.__engine = create_engine(
                # 'mysql+mysqldb://root:@localhost/store_builder'
                'mysql+mysqldb://{}:{}@{}/{}'.format(
                    os.getenv("DB_USERNAME"),
                    os.getenv("DB_PASSWORD"),
                    os.getenv("DB_HOST"),
                    os.getenv("DB_DATABASE"),
                )
            )
            
            
    def reload(self):
        
        
        Base.metadata.create_all(self.__engine)
        self.__session = scoped_session(
            sessionmaker(bind=self.__engine, expire_on_commit=False)
        )