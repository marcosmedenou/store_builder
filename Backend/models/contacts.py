

import os
from typing import Literal
from models.base import BaseModel, Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column
import models

class Contacts(BaseModel, Base):
    
    
    __tablename__ = 'contacts'
    contact_fname : Mapped[str] = mapped_column(String(255))
    contact_lname : Mapped[str] = mapped_column(String(255))
    contact_email : Mapped[str] = mapped_column(String(255))
    
    def __init__(self, contact_fname, contact_lname, contact_email, props = None):
        super().__init__(props)
        self.contact_fname = contact_fname
        self.contact_lname = contact_lname
        self.contact_email = contact_email