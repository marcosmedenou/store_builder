

import os
import json
import models
from datetime import datetime
from sqlalchemy import Integer, DateTime
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped, mapped_column

class Base(DeclarativeBase):
    
    pass

class BaseModel:
    
    
    id : Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    created_at : Mapped[datetime] = mapped_column(DateTime, nullable=False, default=datetime.utcnow())
    
    def __init__(self, props=None):
        
        
        self.created_at = datetime.now()
        if props is not None:
            for key, value in props:
                setattr(self, key, value)
                
                
                
    def bm_update(self, name, value):
        
        setattr(self, name, value)
        
        
    def save(self):
        
        
        models.storage.new(self)
        models.storage.save()